using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.DTOs;


namespace SiCProject.Repositories
{
    public class ProdutoRepository : ProdutoRepositoryInterface
    {

        private readonly SiCContext _context;

        public ProdutoRepository(SiCContext context)
        {
            _context = context;
        }

        public ProdutoDTO getInfoProdutoDTO(Produto p)
        {
            
            //Materiais material = _context.Materiais.Find(p.MaterialID);
            List<String> mats = new List<String>();
            foreach (MaterialProduto materialProd in _context.MateriaisProduto)
            {
                if (materialProd.ProdutoID == p.ID)
                {
                    mats.Add(_context.Materiais.Find(materialProd.MaterialID).Nome);
                }
            }

            List<string> cor = new List<string>();
            int red;
            int green;
            int blue;
            foreach (CorProduto corProd in _context.CoresProduto)
            {
                if (corProd.ProdutoID == p.ID)
                {
                    red = _context.Cores.Find(corProd.CorID).rgbRed;
                    green = _context.Cores.Find(corProd.CorID).rgbVerde;
                    blue = _context.Cores.Find(corProd.CorID).rgbAzul;

                    cor.Add(red + "," + green + "," + blue);
                }
            }

            Dimensao dimensao = _context.Dimensoes.Find(p.DimensaoID);

            List<double> alturas = new List<double>();
            List<double> larguras = new List<double>();
            List<double> profundidades = new List<double>();

            Medida medidaAltura = _context.Medidas.Find(dimensao.alturaID);
            if (medidaAltura is MedidaCont)
            {
                MedidaCont medidaAlturaCont = _context.MedidasCont.Find(dimensao.alturaID);
                alturas.Add(medidaAlturaCont.valor);
                alturas.Add(medidaAlturaCont.valorMax);
            }
            else
            {
                alturas.Add(medidaAltura.valor);
            }

            Medida medidaLargura = _context.Medidas.Find(dimensao.larguraID);
            if (medidaLargura is MedidaCont)
            {
                MedidaCont medidaLarguraCont = _context.MedidasCont.Find(dimensao.larguraID);
                larguras.Add(medidaLarguraCont.valor);
                larguras.Add(medidaLarguraCont.valorMax);
            }
            else
            {
                larguras.Add(medidaLargura.valor);
            }

            Medida medidaProf = _context.Medidas.Find(dimensao.profundidadeID);
            if (medidaProf is MedidaCont)
            {
                MedidaCont medidaProfCont = _context.MedidasCont.Find(dimensao.profundidadeID);
                profundidades.Add(medidaProfCont.valor);
                profundidades.Add(medidaProfCont.valorMax);
            }
            else
            {
                profundidades.Add(medidaProf.valor);
            }

            List<double> restricaoOcupLista = new List<double>();
            List<String> restricoesMat = new List<String>();
            IQueryable<ProdutoRestricao> restricoesProduto = _context.ProdutoRestricoes.Where(pr => pr.ProdutoID.Equals(p.ID));

            foreach (ProdutoRestricao rp in restricoesProduto)
            {
                var restricao = _context.Restricoes.Find(rp.RestricaoID);

                if (restricao is RestricaoOcupacao)
                {
                    var restricaoOcup = _context.RestricoesOcupacao.Find(rp.RestricaoID);
                    restricaoOcupLista.Add(restricaoOcup.limMin);
                    restricaoOcupLista.Add(restricaoOcup.limMax);
                }
                if (restricao is RestricaoMaterial)
                {
                    var restricaoMaterial = _context.RestricoesMaterial.Find(rp.RestricaoID);
                    restricoesMat.Add(restricaoMaterial.material);
                }
            }

            List<int> filhos = new List<int>();
            IQueryable<Composicao> composicaoPai = _context.Composicoes.Where(c => c.ProdutoPaiID.Equals(p.ID));

            foreach (Composicao comp in composicaoPai)
            {
                if (comp.Obrigatoria)
                {
                    filhos.Add(comp.ProdutoFilhoID);
                }
            }

            return new ProdutoDTO(p.ID, p.Nome, p.Preço, p.CategoriaID,
                    mats, cor, alturas, larguras, profundidades,
                    restricaoOcupLista, restricoesMat, filhos);
        }

        public List<Produto> GetAll()
        {
            List<Produto> lista = new List<Produto>();
            foreach (Produto produto in _context.Produtos)
            {
                lista.Add(produto);
            }
            return lista;
        }

        public Produto GetByID(int id)
        {
            Produto produto = _context.Produtos.Find(id);

            return produto;
        }

        public List<Produto> GetByNome(string nome)
        {
            IQueryable<Produto> produto = _context.Produtos.Where(p => p.Nome.Equals(nome));//.FirstOrDefault<Produto>();
            List<Produto> lista = produto.ToList();

            return lista;
        }

        public List<Produto> GetPartes(int id)
        {
            List<Produto> lista = new List<Produto>();

            IQueryable<Composicao> composicoes = _context.Composicoes.Where(c => c.ProdutoPaiID.Equals(id));
            foreach (Composicao c in composicoes)
            {
                Produto produto = _context.Produtos.Find(c.ProdutoFilhoID);
                //alteraValores(produto);
                lista.Add(produto);
            }
            return lista;
        }
        public List<Produto> GetPartesEm(int id)
        {
            List<Produto> lista = new List<Produto>();

            IQueryable<Composicao> composicoes = _context.Composicoes.Where(c => c.ProdutoFilhoID.Equals(id));
            foreach (Composicao c in composicoes)
            {
                Produto produto = _context.Produtos.Find(c.ProdutoPaiID);
                //alteraValores(produto);
                lista.Add(produto);
            }
            return lista;
        }

        public List<String> getCoresProduto(int id)
        {
            List<String> lista = new List<String>();
            IQueryable<CorProduto> corProduto = _context.CoresProduto.Where(c => c.ProdutoID.Equals(id));
            foreach (CorProduto c in corProduto)
            {
                Cor cor = _context.Cores.Find(c.CorID);
                string ze = "rgba(" + cor.rgbRed + "," + cor.rgbVerde + "," + cor.rgbAzul + ",1)";
                lista.Add(ze);
            }
            return lista;
        }
        public Produto Post(Produto obj)
        {
            _context.Produtos.Add(obj);

            _context.SaveChangesAsync();

            return obj;
        }

        public bool Put(Produto obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var produto = _context.Produtos.Find(id);

            if (produto == null)
            {
                return false;
            }

            _context.Produtos.Remove(produto);
            _context.SaveChangesAsync();

            return true;
        }

        /* public bool DeleteCor(int id)
        {
            IQueryable<CorProduto> corProduto = _context.CoresProduto.Where(c => c.ProdutoID.Equals(id));

            if (corProduto == null)
            {
                return false;
            }

            _context.CoresProduto.Remove(corProduto);
            _context.SaveChangesAsync();

            return true;
        }
*/

        private bool ProdutoExists(int id)
        {
            return _context.Produtos.Any(e => e.ID == id);
        }
    }
}