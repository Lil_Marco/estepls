using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;

namespace SiCProject.Repositories
{
    public interface ProdutoRepositoryInterface : Repository<Produto>
    {
        //List<Produto> GetAll();
        //Produto GetByID(int id);
        //bool Put(Produto obj);
        //Produto Post(Produto obj);
        //bool Delete(int id);
        List<Produto> GetByNome(string nome);
        List<Produto> GetPartes(int id);
        List<Produto> GetPartesEm(int id);
    }
}