﻿using Microsoft.EntityFrameworkCore;
using SiCProject.DTOs;
using SiCProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiCProject.Repositories
{
    public class CatalogoRepository : ICatalogoRepository
    {

        private readonly SiCContext _context;
        private ProdutoRepository rep;

        public CatalogoRepository(SiCContext context)
        {
            _context = context;
            rep = new ProdutoRepository(context);
        }

   

        public CatalogoDTO getInfoCotalogoDTO(Catalogo p)
        {
            List<ProdutoDTO> meusProdutos = new List<ProdutoDTO>();

            foreach (CatalogoProduto cp in _context.CatalogoProduto)
            {
                if (cp.CatalogoID == p.ID)
                {
                    meusProdutos.Add(rep.getInfoProdutoDTO(_context.Produtos.Find(cp.ID)));
                }
            }
            return new CatalogoDTO(p.ID, p.nome, meusProdutos);
        }

        public List<Catalogo> GetAll()
        {
            List<Catalogo> cor = new List<Catalogo>();
            foreach (Catalogo corr in _context.Catalogo)
            {
                cor.Add(this.GetByID(corr.ID));
            }
            return cor;
        }

        public Catalogo GetByID(int id)
        {
            Catalogo cat = _context.Catalogo.Find(id);



            return cat;
        }



        public Catalogo Post(Catalogo obj)
        {
            _context.Catalogo.Add(obj);

            _context.SaveChangesAsync();

            return obj;
        }

        public bool Put(Catalogo obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var cat = _context.Catalogo.Find(id);

            if (cat == null)
            {
                return false;
            }

            _context.Catalogo.Remove(cat);
            _context.SaveChangesAsync();

            return true;
        }
    }
}
