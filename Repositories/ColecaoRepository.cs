﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SiCProject.DTOs;
using SiCProject.Models;

namespace SiCProject.Repositories
{
    public class ColecaoRepository : IColecaoRepository
    {

        private readonly SiCContext _context;
        private ProdutoRepository _repository;

        public ColecaoRepository(SiCContext context)
        {
            _context = context;
            _repository = new ProdutoRepository(_context);
        }

        public ColecaoDTO getInfoColecaoDTO(Colecao c)
        {
            List<ProdutoDTO> meusProdutos = new List<ProdutoDTO>();

            foreach(ColecaoProduto cp in _context.ColecaoProdutos)
            {
                if(cp.ColecaoID == c.ID)
                {
                    meusProdutos.Add(_repository.getInfoProdutoDTO(_context.Produtos.Find(cp.ID)));
                }
            }
            LinhaEstetica le = _context.LinhasEsteticas.Find(c.linhaEsteticaID);
            return new ColecaoDTO(c.ID, le.Nome, meusProdutos);
        }

        public List<Colecao> GetAll()
        {
           List<Colecao> cols = new List<Colecao>();       
            foreach(Colecao c in _context.Colecao){
                cols.Add(c);
            }
            return cols;
        }

        public Colecao GetByID(int id)
        {
            Colecao col = _context.Colecao.Find(id);

            return col;
        }

        public Colecao Post(Colecao obj)
        {
            _context.Colecao.Add(obj);

            _context.SaveChangesAsync();

            return obj;
        }

        public bool Put(Colecao obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var col = _context.Colecao.Find(id);

            if (col == null)
            {
                return false;
            }

            _context.Colecao.Remove(col);
            _context.SaveChangesAsync();

            return true;
        }

        private bool ColecaoExists(int id)
        {
            return _context.Colecao.Any(e => e.ID == id);
        }
    }
}
