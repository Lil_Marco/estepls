using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;

namespace SiCProject.Repositories
{
    public interface MaterialRepositoryInterface : Repository<Material>
    {
        //List<Material> GetAll();
        //Material GetByID(int id);
        //bool Put(Material obj);
        //Material Post(Material obj);
        //bool Delete(int id);
        List<Material> GetByNome(string nome);
    }
}