using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColecaoProdutoController : ControllerBase
    {
        private readonly SiCContext _context;

        public ColecaoProdutoController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/ColecaoProduto
        [HttpGet]
        public IEnumerable<ColecaoProduto> GetColecaoProdutos()
        {
            return _context.ColecaoProdutos;
        }

        // GET: api/ColecaoProduto/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetColecaoProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var colecaoProduto = await _context.ColecaoProdutos.FindAsync(id);

            if (colecaoProduto == null)
            {
                return NotFound();
            }

            return Ok(colecaoProduto);
        }

        // PUT: api/ColecaoProduto/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutColecaoProduto([FromRoute] int id, [FromBody] ColecaoProduto colecaoProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != colecaoProduto.ID)
            {
                return BadRequest();
            }

            _context.Entry(colecaoProduto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ColecaoProdutoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ColecaoProduto
        [HttpPost]
        public async Task<IActionResult> PostColecaoProduto([FromBody] ColecaoProduto colecaoProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ColecaoProdutos.Add(colecaoProduto);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetColecaoProduto", new { id = colecaoProduto.ID }, colecaoProduto);
        }

        // DELETE: api/ColecaoProduto/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteColecaoProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var colecaoProduto = await _context.ColecaoProdutos.FindAsync(id);
            if (colecaoProduto == null)
            {
                return NotFound();
            }

            _context.ColecaoProdutos.Remove(colecaoProduto);
            await _context.SaveChangesAsync();

            return Ok(colecaoProduto);
        }

        private bool ColecaoProdutoExists(int id)
        {
            return _context.ColecaoProdutos.Any(e => e.ID == id);
        }
    }
}