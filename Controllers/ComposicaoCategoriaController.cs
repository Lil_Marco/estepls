using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComposicaoCategoriaController : ControllerBase
    {
        private readonly SiCContext _context;

        public ComposicaoCategoriaController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/ComposicaoCategoria
        [HttpGet]
        public IEnumerable<ComposicaoCategoria> GetComposicoesCategorias()
        {
            return _context.ComposicoesCategorias;
        }

        // GET: api/ComposicaoCategoria/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetComposicaoCategoria([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var composicaoCategoria = await _context.ComposicoesCategorias.FindAsync(id);

            if (composicaoCategoria == null)
            {
                return NotFound();
            }

            return Ok(composicaoCategoria);
        }

        // PUT: api/ComposicaoCategoria/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComposicaoCategoria([FromRoute] int id, [FromBody] ComposicaoCategoria composicaoCategoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != composicaoCategoria.ID)
            {
                return BadRequest();
            }

            _context.Entry(composicaoCategoria).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComposicaoCategoriaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ComposicaoCategoria
        [HttpPost]
        public async Task<IActionResult> PostComposicaoCategoria([FromBody] ComposicaoCategoria composicaoCategoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ComposicoesCategorias.Add(composicaoCategoria);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetComposicaoCategoria", new { id = composicaoCategoria.ID }, composicaoCategoria);
        }

        // DELETE: api/ComposicaoCategoria/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteComposicaoCategoria([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var composicaoCategoria = await _context.ComposicoesCategorias.FindAsync(id);
            if (composicaoCategoria == null)
            {
                return NotFound();
            }

            _context.ComposicoesCategorias.Remove(composicaoCategoria);
            await _context.SaveChangesAsync();

            return Ok(composicaoCategoria);
        }

        private bool ComposicaoCategoriaExists(int id)
        {
            return _context.ComposicoesCategorias.Any(e => e.ID == id);
        }
    }
}