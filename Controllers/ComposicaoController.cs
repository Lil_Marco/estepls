using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComposicaoController : ControllerBase
    {
        private readonly SiCContext _context;

        public ComposicaoController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/Composicao
        [HttpGet]
        public IEnumerable<Composicao> GetComposicoes()
        {
            return _context.Composicoes;
        }

        // GET: api/Composicao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetComposicao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var composicao = await _context.Composicoes.FindAsync(id);

            if (composicao == null)
            {
                return NotFound();
            }

            return Ok(composicao);
        }

        [HttpGet("{idPai}/{idFilho}")]
        public async Task<IActionResult> GetComposicao([FromRoute] int idPai, [FromRoute] int idFilho)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var composicao = _context.Composicoes.Where(c=>c.ProdutoFilhoID.Equals(idFilho) &&
                                                            c.ProdutoPaiID.Equals(idPai));
            if (composicao == null)
            {
                return NotFound();
            }

            return Ok(composicao);
        }

        // PUT: api/Composicao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComposicao([FromRoute] int id, [FromBody] Composicao composicao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != composicao.ID)
            {
                return BadRequest();
            }

            _context.Entry(composicao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComposicaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Composicao
        [HttpPost]
        public async Task<IActionResult> PostComposicao([FromBody] Composicao composicao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            bool flagCaber = false;
            bool flagOcupacao = true;
            bool flagMaterial = true;

            var pai = await _context.Produtos.FindAsync(composicao.ProdutoPaiID);
            var filho = await _context.Produtos.FindAsync(composicao.ProdutoFilhoID);

            var dimensaopai = await _context.Dimensoes.FindAsync(pai.DimensaoID);
            var dimensaofilho = await _context.Dimensoes.FindAsync(filho.DimensaoID);

            var alturapai = await _context.Medidas.FindAsync(dimensaopai.alturaID);
            var alturafilho = await _context.Medidas.FindAsync(dimensaofilho.alturaID);

            var largurapai = await _context.Medidas.FindAsync(dimensaopai.larguraID);
            var largurafilho = await _context.Medidas.FindAsync(dimensaofilho.larguraID);

            
            var profundidadepai = await _context.Medidas.FindAsync(dimensaopai.profundidadeID);
            var profundidadefilho = await _context.Medidas.FindAsync(dimensaofilho.profundidadeID);

            //RESTRICAO CABER
            if(alturapai is MedidaCont && largurapai is MedidaCont && profundidadepai is MedidaCont){
                var alturapaiCont = await _context.MedidasCont.FindAsync(dimensaopai.alturaID);
                var largurapaiCont = await _context.MedidasCont.FindAsync(dimensaopai.larguraID);
                var profundidadepaiCont = await _context.MedidasCont.FindAsync(dimensaopai.profundidadeID);
                if(Serviço.restricaoCaber(alturapaiCont.valorMax, alturafilho.valor, largurapaiCont.valorMax, largurafilho.valor, profundidadepaiCont.valorMax, profundidadefilho.valor)){
                    flagCaber = true;           
                }

            }else if(alturapai is MedidaCont && largurapai is MedidaCont){
                var alturapaiCont = await _context.MedidasCont.FindAsync(dimensaopai.alturaID);
                var largurapaiCont = await _context.MedidasCont.FindAsync(dimensaopai.larguraID);
                if(Serviço.restricaoCaber(alturapaiCont.valorMax, alturafilho.valor, largurapaiCont.valorMax, largurafilho.valor, profundidadepai.valor, profundidadefilho.valor)){
                    flagCaber = true;           
                }

            }else if(largurapai is MedidaCont && profundidadepai is MedidaCont){
                var largurapaiCont = await _context.MedidasCont.FindAsync(dimensaopai.larguraID);
                var profundidadepaiCont = await _context.MedidasCont.FindAsync(dimensaopai.profundidadeID);
                if(Serviço.restricaoCaber(alturapai.valor, alturafilho.valor, largurapaiCont.valorMax, largurafilho.valor, profundidadepaiCont.valorMax, profundidadefilho.valor)){
                    flagCaber = true;           
                }

            }else if(alturapai is MedidaCont && profundidadepai is MedidaCont){
                var alturapaiCont = await _context.MedidasCont.FindAsync(dimensaopai.alturaID);
                var profundidadepaiCont = await _context.MedidasCont.FindAsync(dimensaopai.profundidadeID);
                if(Serviço.restricaoCaber(alturapaiCont.valorMax, alturafilho.valor, largurapai.valor, largurafilho.valor, profundidadepaiCont.valorMax, profundidadefilho.valor)){
                    flagCaber = true;           
                }

            }else if(alturapai is MedidaCont){
                var alturapaiCont = await _context.MedidasCont.FindAsync(dimensaopai.alturaID);
                if(Serviço.restricaoCaber(alturapaiCont.valorMax, alturafilho.valor, largurapai.valor, largurafilho.valor, profundidadepai.valor, profundidadefilho.valor)){
                    flagCaber = true;           
                }

            }else if(largurapai is MedidaCont){
                var largurapaiCont = await _context.MedidasCont.FindAsync(dimensaopai.larguraID);
                if(Serviço.restricaoCaber(alturapai.valor, alturafilho.valor, largurapaiCont.valorMax, largurafilho.valor, profundidadepai.valor, profundidadefilho.valor)){
                    flagCaber = true;           
                }

            }else if(profundidadepai is MedidaCont){
                var profundidadepaiCont = await _context.MedidasCont.FindAsync(dimensaopai.profundidadeID);
                if(Serviço.restricaoCaber(alturapai.valor, alturafilho.valor, largurapai.valor, largurafilho.valor, profundidadepaiCont.valorMax, profundidadefilho.valor)){
                    flagCaber = true;           
                }

            }
            else{
                if(Serviço.restricaoCaber(alturapai.valor, alturafilho.valor, largurapai.valor, largurafilho.valor, profundidadepai.valor, profundidadefilho.valor)){
                    flagCaber = true;           
                }
            }

            //RESTRICÕES DO PAI
            IQueryable<ProdutoRestricao> restricoesProd = _context.ProdutoRestricoes.Where(pR=>pR.ProdutoID.Equals(pai.ID));
            List<ProdutoRestricao> lista = restricoesProd.ToList();

            //MATERIAIS DO FILHO
            IQueryable<MaterialProduto> materiaisFilho = _context.MateriaisProduto.Where(mP=>mP.ProdutoID.Equals(filho.ID));
            List<MaterialProduto> listaMateriaisFilho = materiaisFilho.ToList();

            foreach(ProdutoRestricao produtoRestricao in lista){
                var restricao = await _context.Restricoes.FindAsync(produtoRestricao.RestricaoID);
                //Verifica se a restricao é do tipo volume
                if(restricao is RestricaoOcupacao){
                    flagOcupacao = false;
                    var restricaoOcupacaoPai = await _context.RestricoesOcupacao.FindAsync(produtoRestricao.RestricaoID);

                    if(Serviço.restricaoOcupacao(restricaoOcupacaoPai,
                    /*volume pai */Serviço.getVolume(alturapai.valor, largurapai.valor, profundidadepai.valor),
                    /*volume filho */Serviço.getVolume(alturafilho.valor, largurafilho.valor, profundidadefilho.valor))){
                        flagOcupacao = true;
                    }
                }
                //Verifica se a restricao é do tipo material
                if(restricao is RestricaoMaterial){
                    var restricaoMaterialPai = await _context.RestricoesMaterial.FindAsync(produtoRestricao.RestricaoID);

                    foreach(MaterialProduto matProdFilho in listaMateriaisFilho){
                        var materialFilho = await _context.Materiais.FindAsync(matProdFilho.MaterialID);

                        if(Serviço.restricaoMaterial(restricaoMaterialPai, restricaoMaterialPai.material, materialFilho.Nome)){
                            flagMaterial = false;
                        }
                    }
                }
            }

            if(flagCaber && flagMaterial && flagOcupacao){
                _context.Composicoes.Add(composicao);
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetComposicao", new { id = composicao.ID }, composicao);

            }else if(flagMaterial==false){
                throw new MateriaisDiferentesException("O filho tem materiais que são restringidos pelo pai!");

            }else if(flagOcupacao==false){
                throw new PercentagemOcupacaoException("O filho não respeita as percentagens de ocupação do pai!");
            
            }else if(flagCaber==false){
                throw new NaoCabeException("O filho que está a ser inserido não cabe no produto pai!");
            
            }else{
                return NoContent();
            }
        }

        // DELETE: api/Composicao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteComposicao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var composicao = await _context.Composicoes.FindAsync(id);
            if (composicao == null)
            {
                return NotFound();
            }

            _context.Composicoes.Remove(composicao);
            await _context.SaveChangesAsync();

            return Ok(composicao);
        }

        private bool ComposicaoExists(int id)
        {
            return _context.Composicoes.Any(e => e.ID == id);
        }
    }
}