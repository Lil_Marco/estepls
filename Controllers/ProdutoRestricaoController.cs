using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoRestricaoController : ControllerBase
    {
        private readonly SiCContext _context;

        public ProdutoRestricaoController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/ProdutoRestricao
        [HttpGet]
        public IEnumerable<ProdutoRestricao> GetProdutoRestricao()
        {
            return _context.ProdutoRestricoes;
        }

        // GET: api/ProdutoRestricao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProdutoRestricao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produtoRestricao = await _context.ProdutoRestricoes.FindAsync(id);

            if (produtoRestricao == null)
            {
                return NotFound();
            }

            return Ok(produtoRestricao);
        }

        // PUT: api/ProdutoRestricao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProdutoRestricao([FromRoute] int id, [FromBody] ProdutoRestricao produtoRestricao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != produtoRestricao.ID)
            {
                return BadRequest();
            }

            _context.Entry(produtoRestricao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProdutoRestricaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProdutoRestricao
        [HttpPost]
        public async Task<IActionResult> PostProdutoRestricao([FromBody] ProdutoRestricao produtoRestricao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ProdutoRestricoes.Add(produtoRestricao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProdutoRestricao", new { id = produtoRestricao.ID }, produtoRestricao);
        }

        // DELETE: api/ProdutoRestricao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProdutoRestricao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produtoRestricao = await _context.ProdutoRestricoes.FindAsync(id);
            if (produtoRestricao == null)
            {
                return NotFound();
            }

            _context.ProdutoRestricoes.Remove(produtoRestricao);
            await _context.SaveChangesAsync();

            return Ok(produtoRestricao);
        }

        private bool ProdutoRestricaoExists(int id)
        {
            return _context.ProdutoRestricoes.Any(e => e.ID == id);
        }
    }
}