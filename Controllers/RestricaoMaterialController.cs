using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestricaoMaterialController : ControllerBase
    {
        private readonly SiCContext _context;

        public RestricaoMaterialController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/RestricaoMaterial
        [HttpGet]
        public IEnumerable<RestricaoMaterial> GetRestricaoMaterial()
        {
            return _context.RestricoesMaterial;
        }

        // GET: api/RestricaoMaterial/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRestricaoMaterial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var restricaoMaterial = await _context.RestricoesMaterial.FindAsync(id);

            if (restricaoMaterial == null)
            {
                return NotFound();
            }

            return Ok(restricaoMaterial);
        }

        // PUT: api/RestricaoMaterial/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRestricaoMaterial([FromRoute] int id, [FromBody] RestricaoMaterial restricaoMaterial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != restricaoMaterial.ID)
            {
                return BadRequest();
            }

            _context.Entry(restricaoMaterial).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RestricaoMaterialExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RestricaoMaterial
        [HttpPost]
        public async Task<IActionResult> PostRestricaoMaterial([FromBody] RestricaoMaterial restricaoMaterial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.RestricoesMaterial.Add(restricaoMaterial);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRestricaoMaterial", new { id = restricaoMaterial.ID }, restricaoMaterial);
        }

        // DELETE: api/RestricaoMaterial/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRestricaoMaterial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var restricaoMaterial = await _context.RestricoesMaterial.FindAsync(id);
            if (restricaoMaterial == null)
            {
                return NotFound();
            }

            _context.RestricoesMaterial.Remove(restricaoMaterial);
            await _context.SaveChangesAsync();

            return Ok(restricaoMaterial);
        }

        private bool RestricaoMaterialExists(int id)
        {
            return _context.RestricoesMaterial.Any(e => e.ID == id);
        }
    }
}