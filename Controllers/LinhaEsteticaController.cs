using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinhaEsteticaController : ControllerBase
    {
        private readonly SiCContext _context;

        public LinhaEsteticaController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/LinhaEstetica
        [HttpGet]
        public IEnumerable<LinhaEstetica> GetLinhasEsteticas()
        {
            return _context.LinhasEsteticas;
        }

        // GET: api/LinhaEstetica/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLinhaEstetica([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var linhaEstetica = await _context.LinhasEsteticas.FindAsync(id);

            if (linhaEstetica == null)
            {
                return NotFound();
            }

            return Ok(linhaEstetica);
        }

        // PUT: api/LinhaEstetica/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLinhaEstetica([FromRoute] int id, [FromBody] LinhaEstetica linhaEstetica)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != linhaEstetica.ID)
            {
                return BadRequest();
            }

            _context.Entry(linhaEstetica).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LinhaEsteticaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LinhaEstetica
        [HttpPost]
        public async Task<IActionResult> PostLinhaEstetica([FromBody] LinhaEstetica linhaEstetica)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.LinhasEsteticas.Add(linhaEstetica);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLinhaEstetica", new { id = linhaEstetica.ID }, linhaEstetica);
        }

        // DELETE: api/LinhaEstetica/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLinhaEstetica([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var linhaEstetica = await _context.LinhasEsteticas.FindAsync(id);
            if (linhaEstetica == null)
            {
                return NotFound();
            }

            _context.LinhasEsteticas.Remove(linhaEstetica);
            await _context.SaveChangesAsync();

            return Ok(linhaEstetica);
        }

        private bool LinhaEsteticaExists(int id)
        {
            return _context.LinhasEsteticas.Any(e => e.ID == id);
        }
    }
}