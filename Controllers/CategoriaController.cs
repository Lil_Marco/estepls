using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.DTOs;
using SiCProject.Models;
using SiCProject.Repositories;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        private readonly SiCContext _context;

        private CategoriaRepository _repository;

        #pragma warning disable 1998
        public CategoriaController(SiCContext context)
        {
            _context = context;
            _repository = new CategoriaRepository(_context);
        }

       /* private async void getSubCategorias(Categoria categoria){

            categoria.SubCategorias = new List<String>();
            foreach(ComposicaoCategoria comp in _context.ComposicoesCategorias){
                if(categoria.ID == comp.CategoriaPaiID){
                    var cat = await _context.Categorias.FindAsync(comp.CategoriaFilhoID);
                    cat.SubCategorias = new List<String>();
                    categoria.SubCategorias.Add(cat.nome);
                }
            }
        }
        */
        #pragma warning disable 1998
        // GET: api/Categoria
        [HttpGet]
        public async Task<IActionResult> GetCategorias()
        {
            List<Categoria> lista = _repository.GetAll();

            List<CategoriaDTO> dtos = new List<CategoriaDTO>();

            foreach(Categoria mat in lista){
                dtos.Add(_repository.getInfoCategoriaDTO(mat));
            }

            return Ok(dtos);
        }

        // GET: api/Categoria/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategoriaByID([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Categoria categoria = _repository.GetByID(id);

            if (categoria == null)
            {
                return NotFound();
            }

            return Ok(_repository.getInfoCategoriaDTO(categoria));
        }

        // PUT: api/Categoria/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategoria([FromRoute] int id, [FromBody] Categoria categoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categoria.ID)
            {
                return BadRequest();
            }

            if(_repository.Put(categoria)){
                 return NoContent();
            }else{
                return NotFound();
            }
        }

        // POST: api/Categoria
        [HttpPost]
        public async Task<IActionResult> PostCategoria([FromBody] Categoria categoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

           Categoria cat = _repository.Post(categoria);

            return Ok(_repository.getInfoCategoriaDTO(cat));
        }

        // DELETE: api/Categoria/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategoria([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(_repository.Delete(id)){
                return NoContent();
            }else{
                return NotFound();
            }
        }

        private bool CategoriaExists(int id)
        {
            return _context.Categorias.Any(e => e.ID == id);
        }
    }
}