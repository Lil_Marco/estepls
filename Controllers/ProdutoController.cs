using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Repositories;
using SiCProject.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace SiCProject.Controllers
{
  //  [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly SiCContext _context;
        private ProdutoRepository _repository;

        #pragma warning disable 1998

        public ProdutoController(SiCContext context)
        {
            _context = context;
            _repository = new ProdutoRepository(_context);
        }

        // GET: api/Produto
        [HttpGet]
        public async Task<IActionResult> GetProdutos()
        {   
            List<Produto> lista = _repository.GetAll();

            List<ProdutoDTO> dtos = new List<ProdutoDTO>();

            foreach(Produto p in lista){
                dtos.Add(_repository.getInfoProdutoDTO(p));
            }
            
            return Ok(dtos);
        }

        // GET: api/Produto/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProdutoByID([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Produto produto = _repository.GetByID(id);

            if (produto == null)
            {
                return NotFound();
            }

            return Ok(_repository.getInfoProdutoDTO(produto));
        }

        [HttpGet("nome={nome}")]
        public async Task<IActionResult> GetProdutoByName([FromRoute] string nome)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var produto = await _context.Produtos.FindAsync(id);
            List<Produto> lista = _repository.GetByNome(nome);

            if (lista==null)
            {
                return NotFound();
            }

            List<ProdutoDTO> dtos = new List<ProdutoDTO>();
            foreach(Produto p in lista){
                dtos.Add(_repository.getInfoProdutoDTO(p));
            }
            return Ok(dtos);
        }

        [HttpGet("Cor/{id}")]
        public async Task<IActionResult> GetProdutoByName([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var produto = await _context.Produtos.FindAsync(id);
            List<string> lista = _repository.getCoresProduto(id);

            if (lista==null)
            {
                return NotFound();
            }

            
            return Ok(lista);
        }

        // PUT: api/Produto/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduto([FromRoute] int id, [FromBody] Produto produto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != produto.ID)
            {
                return BadRequest();
            }

            if(_repository.Put(produto)){
                 return NoContent();
            }else{
                return NotFound();
            }

            /*------------------- */
        }

        // POST: api/Produto
        [HttpPost]
        public async Task<IActionResult> PostProduto([FromBody] Produto produto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Produto prod = _repository.Post(produto);

            return Ok(_repository.getInfoProdutoDTO(prod));
        }

        // GET: api/Produto/{id}/Partes
        [HttpGet("{id}/Partes")]
        public async Task<IActionResult> GetPartes([FromRoute] int id)
        {
            List<ProdutoDTO> lista = new List<ProdutoDTO>();

            foreach(Produto p in _repository.GetPartes(id)){
                lista.Add(_repository.getInfoProdutoDTO(p));
            }
            
            IEnumerable<ProdutoDTO> listaFinal = lista;
            return Ok(listaFinal);
        }

        // GET: api/Produto/{id}/PartesEm
        [HttpGet("{id}/PartesEm")]
        public async Task<IActionResult> GetPartesEm([FromRoute] int id)
        {
            List<ProdutoDTO> lista = new List<ProdutoDTO>();

            foreach(Produto p in _repository.GetPartesEm(id)){
                lista.Add(_repository.getInfoProdutoDTO(p));
            }

            IEnumerable<ProdutoDTO> listaFinal = lista;
            return Ok(listaFinal);
        }

        // DELETE: api/Produto/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }      
            var produto = new Produto();
            if(_repository.Delete(id)){
                return NoContent();
            }else{
                return NotFound();
            }
        }
        private bool ProdutoExists(int id)
        {
            return _context.Produtos.Any(e => e.ID == id);
        }
    }
}