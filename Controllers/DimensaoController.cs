using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DimensaoController : ControllerBase
    {
        private readonly SiCContext _context;

        public DimensaoController(SiCContext context)
        {
            _context = context;
        }

        public DimensaoDTO getInfoDimensaoDTO(Dimensao dimensao){
            
            List<double> alturas = new List<double>();
            List<double> larguras = new List<double>();
            List<double> profundidades = new List<double>();

            Medida medidaAltura = _context.Medidas.Find(dimensao.alturaID);
            if(medidaAltura is MedidaCont){
                MedidaCont medidaAlturaCont = _context.MedidasCont.Find(dimensao.alturaID);
                alturas.Add(medidaAlturaCont.valor);
                alturas.Add(medidaAlturaCont.valorMax);
            }else{
                alturas.Add(medidaAltura.valor);
            }

            Medida medidaLargura = _context.Medidas.Find(dimensao.larguraID);
            if(medidaLargura is MedidaCont){
                MedidaCont medidaLarguraCont = _context.MedidasCont.Find(dimensao.larguraID);
                larguras.Add(medidaLarguraCont.valor);
                larguras.Add(medidaLarguraCont.valorMax);
            } else{
                larguras.Add(medidaLargura.valor);
            }

            Medida medidaProf = _context.Medidas.Find(dimensao.profundidadeID);
            if(medidaProf is MedidaCont){
                MedidaCont medidaProfCont = _context.MedidasCont.Find(dimensao.profundidadeID);
                profundidades.Add(medidaProfCont.valor);
                profundidades.Add(medidaProfCont.valorMax);
            } else{
                profundidades.Add(medidaProf.valor);
            }

            return new DimensaoDTO(dimensao.ID, alturas, larguras, profundidades);
        }

        // GET: api/Dimensao
        [HttpGet]
        public async Task<IActionResult> GetDimensoes()
        {
            List<DimensaoDTO> dtos = new List<DimensaoDTO>();

            foreach(Dimensao d in _context.Dimensoes){
                dtos.Add(getInfoDimensaoDTO(d));
            }
            
            return Ok(dtos);
        }

        // GET: api/Dimensao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDimensao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dimensao = await _context.Dimensoes.FindAsync(id);

            if (dimensao == null)
            {
                return NotFound();
            }

            return Ok(getInfoDimensaoDTO(dimensao));
        }

        // PUT: api/Dimensao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDimensao([FromRoute] int id, [FromBody] Dimensao dimensao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dimensao.ID)
            {
                return BadRequest();
            }

            _context.Entry(dimensao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DimensaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Dimensao
        [HttpPost]
        public async Task<IActionResult> PostDimensao([FromBody] Dimensao dimensao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Dimensoes.Add(dimensao);
            await _context.SaveChangesAsync();

            return Ok(getInfoDimensaoDTO(dimensao));
        }

        // DELETE: api/Dimensao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDimensao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dimensao = await _context.Dimensoes.FindAsync(id);
            if (dimensao == null)
            {
                return NotFound();
            }

            _context.Dimensoes.Remove(dimensao);
            await _context.SaveChangesAsync();

            return Ok(dimensao);
        }

        private bool DimensaoExists(int id)
        {
            return _context.Dimensoes.Any(e => e.ID == id);
        }
    }
}