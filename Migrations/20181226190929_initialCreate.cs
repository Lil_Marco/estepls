﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiCProject.Migrations
{
    public partial class initialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Catalogo",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Catalogo", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CatalogoProduto",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ProdutoID = table.Column<int>(nullable: false),
                    CatalogoID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CatalogoProduto", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Categorias",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    nome = table.Column<string>(nullable: true),
                    CategoriaID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categorias", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Categorias_Categorias_CategoriaID",
                        column: x => x.CategoriaID,
                        principalTable: "Categorias",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ColecaoProdutos",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ProdutoID = table.Column<int>(nullable: false),
                    ColecaoID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ColecaoProdutos", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Composicoes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Obrigatoria = table.Column<bool>(nullable: false),
                    ProdutoPaiID = table.Column<int>(nullable: false),
                    ProdutoFilhoID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Composicoes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ComposicoesCategorias",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CategoriaPaiID = table.Column<int>(nullable: false),
                    CategoriaFilhoID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComposicoesCategorias", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CoresProduto",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ProdutoID = table.Column<int>(nullable: false),
                    CorID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoresProduto", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "LinhasEsteticas",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinhasEsteticas", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "MateriaisProduto",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ProdutoID = table.Column<int>(nullable: false),
                    MaterialID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MateriaisProduto", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Medidas",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    valor = table.Column<double>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    valorMax = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medidas", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ProdutoRestricoes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ProdutoID = table.Column<int>(nullable: false),
                    RestricaoID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProdutoRestricoes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Restricoes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Discriminator = table.Column<string>(nullable: false),
                    material = table.Column<string>(nullable: true),
                    limMin = table.Column<double>(nullable: true),
                    limMax = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Restricoes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Tets",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ola = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tets", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    role = table.Column<string>(nullable: true),
                    Token = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Colecao",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    linhaEsteticaID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Colecao", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Colecao_LinhasEsteticas_linhaEsteticaID",
                        column: x => x.linhaEsteticaID,
                        principalTable: "LinhasEsteticas",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Dimensoes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    alturaID = table.Column<int>(nullable: false),
                    larguraID = table.Column<int>(nullable: false),
                    profundidadeID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dimensoes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Dimensoes_Medidas_alturaID",
                        column: x => x.alturaID,
                        principalTable: "Medidas",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Dimensoes_Medidas_larguraID",
                        column: x => x.larguraID,
                        principalTable: "Medidas",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Dimensoes_Medidas_profundidadeID",
                        column: x => x.profundidadeID,
                        principalTable: "Medidas",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Produtos",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(nullable: true),
                    Preço = table.Column<float>(nullable: false),
                    CategoriaID = table.Column<int>(nullable: false),
                    DimensaoID = table.Column<int>(nullable: false),
                    CatalogoID = table.Column<int>(nullable: true),
                    ColecaoID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produtos", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Produtos_Catalogo_CatalogoID",
                        column: x => x.CatalogoID,
                        principalTable: "Catalogo",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Produtos_Categorias_CategoriaID",
                        column: x => x.CategoriaID,
                        principalTable: "Categorias",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Produtos_Colecao_ColecaoID",
                        column: x => x.ColecaoID,
                        principalTable: "Colecao",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Produtos_Dimensoes_DimensaoID",
                        column: x => x.DimensaoID,
                        principalTable: "Dimensoes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cores",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(nullable: true),
                    rgbRed = table.Column<int>(nullable: false),
                    rgbVerde = table.Column<int>(nullable: false),
                    rgbAzul = table.Column<int>(nullable: false),
                    ProdutoID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cores", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Cores_Produtos_ProdutoID",
                        column: x => x.ProdutoID,
                        principalTable: "Produtos",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Materiais",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(nullable: true),
                    Preco = table.Column<float>(nullable: false),
                    ProdutoID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Materiais", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Materiais_Produtos_ProdutoID",
                        column: x => x.ProdutoID,
                        principalTable: "Produtos",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Acabamentos",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(nullable: true),
                    Incremento = table.Column<float>(nullable: false),
                    MaterialId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Acabamentos", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Acabamentos_Materiais_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materiais",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Acabamentos_MaterialId",
                table: "Acabamentos",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_Categorias_CategoriaID",
                table: "Categorias",
                column: "CategoriaID");

            migrationBuilder.CreateIndex(
                name: "IX_Colecao_linhaEsteticaID",
                table: "Colecao",
                column: "linhaEsteticaID");

            migrationBuilder.CreateIndex(
                name: "IX_Cores_ProdutoID",
                table: "Cores",
                column: "ProdutoID");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_alturaID",
                table: "Dimensoes",
                column: "alturaID");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_larguraID",
                table: "Dimensoes",
                column: "larguraID");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_profundidadeID",
                table: "Dimensoes",
                column: "profundidadeID");

            migrationBuilder.CreateIndex(
                name: "IX_Materiais_ProdutoID",
                table: "Materiais",
                column: "ProdutoID");

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_CatalogoID",
                table: "Produtos",
                column: "CatalogoID");

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_CategoriaID",
                table: "Produtos",
                column: "CategoriaID");

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_ColecaoID",
                table: "Produtos",
                column: "ColecaoID");

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_DimensaoID",
                table: "Produtos",
                column: "DimensaoID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Acabamentos");

            migrationBuilder.DropTable(
                name: "CatalogoProduto");

            migrationBuilder.DropTable(
                name: "ColecaoProdutos");

            migrationBuilder.DropTable(
                name: "Composicoes");

            migrationBuilder.DropTable(
                name: "ComposicoesCategorias");

            migrationBuilder.DropTable(
                name: "Cores");

            migrationBuilder.DropTable(
                name: "CoresProduto");

            migrationBuilder.DropTable(
                name: "MateriaisProduto");

            migrationBuilder.DropTable(
                name: "ProdutoRestricoes");

            migrationBuilder.DropTable(
                name: "Restricoes");

            migrationBuilder.DropTable(
                name: "Tets");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Materiais");

            migrationBuilder.DropTable(
                name: "Produtos");

            migrationBuilder.DropTable(
                name: "Catalogo");

            migrationBuilder.DropTable(
                name: "Categorias");

            migrationBuilder.DropTable(
                name: "Colecao");

            migrationBuilder.DropTable(
                name: "Dimensoes");

            migrationBuilder.DropTable(
                name: "LinhasEsteticas");

            migrationBuilder.DropTable(
                name: "Medidas");
        }
    }
}
