﻿using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;

namespace SiCProject.Models
{
    public class SiCContext : DbContext
    {
        public SiCContext(DbContextOptions<SiCContext> options)
            : base(options)
        {
        }

        public DbSet<Produto> Produtos { get; set; }

        public DbSet<Categoria> Categorias { get; set; }

        public DbSet<Composicao> Composicoes { get; set; }

        public DbSet<Material> Materiais { get; set; }


        public DbSet<Acabamento> Acabamentos { get; set; }

        public DbSet<Dimensao> Dimensoes { get; set; }

        public DbSet<ComposicaoCategoria> ComposicoesCategorias { get; set; }

        public DbSet<Medida> Medidas { get; set; }

        public DbSet<MaterialProduto> MateriaisProduto { get; set; }

        public DbSet<MedidaCont> MedidasCont { get; set; }

        public DbSet<Restricoes.Restricao> Restricoes { get; set; }

        public DbSet<RestricaoMaterial> RestricoesMaterial { get; set; }

        public DbSet<RestricaoOcupacao> RestricoesOcupacao { get; set; }

        public DbSet<ProdutoRestricao> ProdutoRestricoes { get; set; }

        public DbSet<Cor> Cores { get; set; }
		
        public DbSet<CorProduto> CoresProduto { get; set; }

        public DbSet<User> Users {get; set;}

        public DbSet<SiCProject.Models.Tets> Tets { get; set; }

        public DbSet<SiCProject.Models.Catalogo> Catalogo { get; set; }

        public DbSet<SiCProject.Models.CatalogoProduto> CatalogoProduto { get; set; }

        public DbSet<SiCProject.Models.Colecao> Colecao { get; set; }

        public DbSet<SiCProject.Models.ColecaoProduto> ColecaoProdutos { get; set; }

        public DbSet<SiCProject.Models.LinhaEstetica> LinhasEsteticas { get; set; }

    }
}