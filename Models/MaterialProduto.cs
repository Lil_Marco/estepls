using System;
using System.Collections.Generic;

namespace SiCProject.Models
{
    public class MaterialProduto
    {
        public int ID {get; set;}

        public int ProdutoID {get; set;}

        public int MaterialID {get; set;}
    }
}