using System;

namespace SiCProject.Models
{
    public class Cor
    {
        public int ID { get; set; }

        public string Nome { get; set; }
        public int rgbRed { get; set; }
        public int rgbVerde { get; set; }
        public int rgbAzul { get; set; }


    }
}