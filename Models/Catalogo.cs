﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiCProject.Models
{
    public class Catalogo
    {


        public int ID { get; set; }

        public string nome { get; set; } 

        public virtual List<Produto> lProdutos { get; set; }
    }
}
