﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiCProject.Models
{
    public class Colecao
    {

        public int ID { get; set; }

        public int linhaEsteticaID { get; set; }
        public LinhaEstetica linhaEstetica {get; set;}

        public virtual List<Produto> lProdutos { get; set; }

    }
}
