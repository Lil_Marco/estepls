using System;

public class User
{
    public int ID { get; set; }

    public string Username { get; set; }
    public string Password { get; set; }

    public string role { get; set; }

    public string Token { get; set; }

    public string email { get; set; }
}