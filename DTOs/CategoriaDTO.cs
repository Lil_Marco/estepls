using System;
using SiCProject.Models;
using System.Collections.Generic;

namespace SiCProject.DTOs
{
    public class CategoriaDTO
    {
        public int ID {get; set;}
        public string Nome {get; set;}
        public List<int> SubCategorias {get; set;}

        public CategoriaDTO(int id, string nomecat, List<int> subcats){
            this.ID = id;
            this.Nome = nomecat;
            this.SubCategorias = subcats;
        }

    }
}