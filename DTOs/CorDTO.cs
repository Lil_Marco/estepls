using System;
using SiCProject.Models;

namespace SiCProject.DTOs
{
    public class CorDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public int rgbRed { get; set; }
        public int rgbVerde { get; set; }
        public int rgbAzul { get; set; }

        public CorDTO(int id, string nomecor, int red, int verde, int azul)
        {
            this.ID = id;
            this.Nome = nomecor;
            this.rgbRed = red;
            this.rgbVerde = verde;
            this.rgbAzul = azul;
        }

    }
}